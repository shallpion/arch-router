#!/bin/sh

# This script is used to clean orphaned kea lock pid files after 
# a possible sudden reboot.

rm -f /tmp/clean-kea-pid.log

file1="/var/kea/kea-dhcp-ddns.kea-dhcp-ddns.pid"

if [ -f $file1 ] ; then
    rm $file1
		echo "done cleaning kea pid $file1" >> /tmp/clean-kea-pid.log
fi

file2="/var/kea/kea-dhcp4.kea-dhcp4.pid"

if [ -f $file2 ] ; then
    rm $file2
		echo "done cleaning kea pid $file2" >> /tmp/clean-kea-pid.log
fi


