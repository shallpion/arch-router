#!/bin/sh

conf=/root/dhcp6.conf
ifname=$1


# To make sure the ipv6 address has been successfully configured on the interface
sleep 1

subnet=$(ip -6 addr show dev $ifname | grep inet6.*global | sed 1q | awk '{print $2}')
echo "woof"
if [ -z "$subnet" ]; then
	exit 0
fi
prefix=$(echo $subnet | sed 's/\(.*\)::.*/\1/')



# Initialize an empty configuration file
if [ ! -f $conf ]; then
	touch $conf
fi
mv $conf $conf.old && sed '/^/d' $conf.old > $conf


cat <<EOF > $conf
# Auto-generated DHCPv6 configuration file
# Do NOT modify this file directly as it will be
# over-written after DHCPv6 server restarts

default-lease-time 1800;
max-lease-time 7200;

subnet6 $prefix::/64 {
	range6 $prefix::1000 $prefix::8000;
}
EOF

